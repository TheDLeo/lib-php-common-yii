<?php
/**
 * DynamicModel.php
 */
namespace DreamFactory\Yii\Models;

/**
 * DynamicModel
 */
class DynamicModel extends BaseFactoryModel
{
	//*************************************************************************
	//* Members
	//*************************************************************************

	/**
	 * @var string The name of the table
	 */
	protected static $_tableName;

	//*************************************************************************
	//* Methods
	//*************************************************************************

	/**
	 * @param string $tableName
	 * @param string $scenario
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct( $tableName, $scenario = 'insert' )
	{
		if ( empty( static::$_tableName ) && empty( $tableName ) )
		{
			throw new \InvalidArgumentException( 'The value for "$tableName" is invalid.' );
		}

		if ( empty( $tableName ) )
		{
			$scenario = null;
		}
		else
		{
			static::$_tableName = $tableName;
		}

		return parent::__construct( $scenario );
	}

	/**
	 * @param string $tableName
	 * @param string $class
	 *
	 * @throws \InvalidArgumentException
	 *
	 * @return \CActiveRecord|void
	 */
	public static function model( $tableName, $class = null )
	{
		if ( empty( static::$_tableName ) && empty( $tableName ) )
		{
			throw new \InvalidArgumentException( 'The value for "$tableName" is invalid.' );
		}

		static::$_tableName = $tableName;

		return $_model = parent::model( $class );
	}

	/**
	 * @return string
	 */
	public function tableName()
	{
		return static::$_tableName;
	}

	/**
	 * @param $tableName
	 *
	 * @return string
	 */
	public static function setTableName( $tableName )
	{
		return static::$_tableName = $tableName;
	}

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return static::$_tableName;
	}
}
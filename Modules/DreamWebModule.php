<?php
/**
 * DreamWebModule.php
 */
namespace DreamFactory\Yii\Modules;

use \DreamFactory\Yii\Utility\Pii;
use Kisma\Core\Utility\Option;

/**
 * DreamWebModule
 * Provides extra functionality to the base Yii module functionality
 */
class DreamWebModule extends \CWebModule
{
	//*************************************************************************
	//* Private Members
	//*************************************************************************

	/**
	 * @var string
	 */
	protected $_configPath = null;
	/**
	 * @var string
	 */
	protected $_assetPath = null;
	/**
	 * @var string
	 */
	protected $_assetUrl = null;

	//*************************************************************************
	//* Methods
	//*************************************************************************

	/**
	 * @param string $name
	 *
	 * @return \CDbConnection
	 */
	public function getDb( $name = 'db' )
	{
		return Pii::db( $name );
	}

	/**
	 * Initialize
	 */
	public function init()
	{
		//	Phone home...
		parent::init();

		//	import the module-level models and components
		$this->setImport(
			array(
				 $this->id . '.models.*',
				 $this->id . '.components.*',
			)
		);

		//	Read private configuration...
		if ( !empty( $this->_configPath ) )
		{
			/** @noinspection PhpIncludeInspection */
			if ( false !== ( $_configuration = require( $this->basePath . $this->_configPath ) ) )
			{
				$this->configure( $_configuration );
			}
		}

		//	Get our asset manager going...
		$this->_setAssetPaths();

		//	Who doesn't need this???
		if ( !Option::get( Pii::clientScript()->scriptMap, 'jquery.js', false ) )
		{
			Pii::clientScript()->registerCoreScript( 'jquery' );
		}
	}

	/**
	 * Initializes the asset manager for this module
	 */
	protected function _setAssetPaths()
	{
		$_assetManager = Pii::app()->getAssetManager();

		if ( null === $this->_assetPath )
		{
			$this->_assetPath = $_assetManager->getBasePath() . DIRECTORY_SEPARATOR . $this->getId();
		}

		if ( !is_dir( $this->_assetPath ) )
		{
			@mkdir( $this->_assetPath );
		}

		$this->_assetUrl = Pii::publishAsset( $this->_assetPath, true );
	}
}
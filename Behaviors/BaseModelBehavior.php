<?php
/**
 * BaseModelBehavior.php
 */
namespace DreamFactory\Yii\Behaviors;

use Kisma\Core\Utility\Option;

/**
 * BaseModelBehavior
 * A base class for AR behaviors
 */
class BaseModelBehavior extends \CActiveRecordBehavior
{
	//********************************************************************************
	//* Methods
	//********************************************************************************

	/**
	 * Constructor
	 */
	function __construct( $settings = array() )
	{
		if ( !empty( $settings ) )
		{
			foreach ( Option::clean( $settings ) as $_key => $_value )
			{
				if ( $this->hasProperty( $_key ) )
				{
					$this->__set( $_key, $_value );
				}
			}
		}
	}
}
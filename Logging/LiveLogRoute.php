<?php
/**
 * LiveLogRoute.php
 *
 * @copyright Copyright (c) 2012 DreamFactory Software, Inc.
 * @link      http://www.dreamfactory.com DreamFactory Software, Inc.
 * @author    Jerry Ablan <jerryablan@dreamfactory.com>
 *
 * @filesource
 */
namespace DreamFactory\Yii\Logging;

use Kisma\Core\Utility\Log;

/**
 * LiveLogRoute
 * Utilizes Kisma centralized logging to write logs in real time
 */
class LiveLogRoute extends \CFileLogRoute
{
	//**************************************************************************
	//* Members
	//**************************************************************************

	/**
	 * @property array An array of categories to exclude from logging. Regex pattern matching is supported via {@link preg_match}
	 */
	protected $_excludedCategories = array();

	//*************************************************************************
	//* Methods
	//*************************************************************************

	/**
	 * Retrieves filtered log messages from logger for further processing.
	 *
	 * @param \CLogger $logger      logger instance
	 * @param boolean  $processLogs whether to process the logs after they are collected from the logger. ALWAYS TRUE NOW!
	 */
	public function collectLogs( $logger, $processLogs = false )
	{
		parent::collectLogs( $logger, true );
	}

	/**
	 * Writes log messages in files.
	 *
	 * @param array $logs list of log messages
	 */
	protected function processLogs( $logs )
	{
		try
		{
			Log::setDefaultLog( $_logFile = $this->getLogPath() . DIRECTORY_SEPARATOR . $this->getLogFile() );

			if ( @filesize( $_logFile ) > $this->getMaxFileSize() * 1024 )
			{
				$this->rotateFiles();
			}

			if ( !is_array( $logs ) )
			{
				return;
			}

			//	Write out the log entries
			foreach ( $logs as $_log )
			{
				$_exclude = false;

				//	Check out the exclusions
				if ( !empty( $this->_excludedCategories ) )
				{
					foreach ( $this->_excludedCategories as $_category )
					{
						//	If found, we skip
						if ( trim( strtolower( $_category ) ) == trim( strtolower( $_log[2] ) ) )
						{
							$_exclude = true;
							break;
						}

						//	Check for regex
						if ( '/' == $_category[0] && 0 != @preg_match( $_category, $_log[2] ) )
						{
							$_exclude = true;
							break;
						}
					}
				}

				/**
				 *     Use {@link error_log} facility to write out log entry
				 */
				if ( !$_exclude )
				{
					/**
					 * 0 = $message
					 * 1 = $level
					 * 2 = $category
					 * 3 = $timestamp
					 */
					list( $_message, $_level, $_category, $_timestamp ) = $_log;

					Log::log( $_message, $_level, null, null, $_category );
				}
			}

			//	Processed, clear!
			$this->logs = null;
		}
		catch ( \Exception $_ex )
		{
			error_log( __METHOD__ . ': Exception processing application logs: ' . $_ex->getMessage() );
		}
	}

	/**
	 * Formats a log message given different fields.
	 *
	 * @param string     $message  message content
	 * @param int|string $level    message level
	 * @param string     $category message category
	 * @param float      $timestamp
	 *
	 * @return string formatted message
	 */
	protected function formatLogMessage( $message, $level = 'info', $category = null, $timestamp = null )
	{
		return @date( 'M j H:i:s', $timestamp ? : time() ) . ' [' . strtoupper( substr( $level, 0, 4 ) ) . '] ' . $message . ' {"category":"' .
				$category . '"}' . PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL;
	}

	/**
	 * @param $excludedCategories
	 *
	 * @return LiveLogRoute
	 */
	public function setExcludedCategories( $excludedCategories )
	{
		$this->_excludedCategories = $excludedCategories;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getExcludedCategories()
	{
		return $this->_excludedCategories;
	}
}

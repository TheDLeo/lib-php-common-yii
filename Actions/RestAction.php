<?php
/**
 * RestAction.php
 */
namespace DreamFactory\Yii\Actions;

use Kisma\Core\Enums\HttpMethod;

/**
 * RestAction
 * Represents a REST action that is defined as a BaseRestController method.
 * The method name is like 'getXYZ' where 'XYZ' stands for the action name.
 */
class RestAction extends \CAction
{
	//********************************************************************************
	//* Members
	//********************************************************************************

	/**
	 * @var mixed The inbound payload for non-GET/POST requests
	 */
	protected $_payload;
	/**
	 * @var string The request method
	 */
	protected $_method;

	//********************************************************************************
	//* Methods
	//********************************************************************************

	/**
	 * @param \CController $controller
	 * @param string       $id
	 * @param string       $method
	 */
	public function __construct( $controller, $id, $method = 'GET' )
	{
		parent::__construct( $controller, $id );

		$this->_method = strtoupper( $method );

		if ( HttpMethod::Get != $this->_method && HttpMethod::Post != $this->_method )
		{
			//	Get the payload...
			$this->_payload = @file_get_contents( 'php://input' );
		}
	}

	/**
	 * Runs the REST action.
	 *
	 * @throws CHttpException
	 */
	public function run()
	{
		$_controller = $this->getController();

		if ( !( $_controller instanceof BaseRestController ) )
		{
			$_controller->missingAction( $this->getId() );

			return;
		}

		//	Call the controllers dispatch method...
		$_controller->dispatchRequest( $this );
	}

	//*************************************************************************
	//* Properties
	//*************************************************************************

	/**
	 * @param mixed $payload
	 *
	 * @return \DreamFactory\Yii\Actions\RestAction
	 */
	public function setPayload( $payload )
	{
		$this->_payload = $payload;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPayload()
	{
		return $this->_payload;
	}

}
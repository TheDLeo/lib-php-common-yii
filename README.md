DreamFactory PHP Yii Components
===============================

This library is a set of components for use with the Yii Framework.

    use DreamFactory\Yii\Utility\Pii;

    if ( Pii::guest() ) {
    	$this->redirect( Pii::user()->loginUrl );
    }

Installation
============

Add a line to your "require" section in your composer configuration:

	"require":           {
		"dreamfactory/lib-php-common-yii": "dev-master"
	}

Run a composer update:

    $ composer update

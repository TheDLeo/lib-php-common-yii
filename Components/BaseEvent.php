<?php
/**
 * BaseEvent.php
 */
namespace DreamFactory\Yii\Components;

/**
 * BaseEvent
 * A more-better event class
 */
use Kisma\Core\Utility\Option;

class BaseEvent extends \CEvent
{
	//********************************************************************************
	//* Public Methods
	//********************************************************************************

	/**
	 * Constructor.
	 *
	 * @param \CComponent $sender
	 * @param mixed       $parameters
	 *
	 * @return \DreamFactory\Yii\Components\BaseEvent
	 */
	public function __construct( $sender = null, $parameters = null )
	{
		$this->sender = $sender;
		$this->params = $parameters;
	}

	/**
	 * @param string     $name
	 * @param mixed|null $defaultValue
	 *
	 * @return mixed
	 */
	public function getParameter( $name, $defaultValue = null )
	{
		return Option::get( $this->params, $name, $defaultValue );
	}
}
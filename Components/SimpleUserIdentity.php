<?php
/**
 * SimpleUserIdentity.php
 */
namespace DreamFactory\Yii\Components;

/**
 * SimpleUserIdentity
 * Provides a password-based login. The allowed users are retrieved from the configuration file in the 'params' section.
 * The array data should be in 'UserName' => 'password' format.
 */
use DreamFactory\Yii\Utility\Pii;

class SimpleUserIdentity extends \CUserIdentity
{
	//*************************************************************************
	//* Members
	//*************************************************************************

	/**
	 * @var int The user ID
	 */
	protected $_userId;

	//*************************************************************************
	//* Methods
	//*************************************************************************

	/**
	 * Authenticates a user.
	 *
	 * @return boolean
	 */
	public function authenticate()
	{
		return ( static::ERROR_NONE === ( $this->errorCode = static::_authenticate( $this->username, $this->password ) ) );
	}

	/**
	 * @param string $userName
	 * @param string $password
	 *
	 * @return int
	 */
	protected static function _authenticate( $userName, $password )
	{
		$_checkUser = trim( strtolower( $userName ) );
		$_checkPassword = trim( $password );

		$_allowedUsers = Pii::getParam( 'app.auth.allowedUsers', array() );

		if ( !isset( $_allowedUsers[$_checkUser] ) )
		{
			return static::ERROR_USERNAME_INVALID;
		}

		if ( $_allowedUsers[$_checkUser] !== $_checkPassword && $_allowedUsers[$_checkUser] !== md5( $_checkPassword ) )
		{
			return static::ERROR_PASSWORD_INVALID;
		}

		return static::ERROR_NONE;
	}

	/**
	 * @return int
	 */
	public function getUserId()
	{
		return $this->_userId;
	}

	/**
	 * Returns the user's ID instead of the name
	 *
	 * @return int|string
	 */
	public function getId()
	{
		return $this->_userId;
	}

	/**
	 * @param int $userId
	 *
	 * @return SimpleUserIdentity
	 */
	public function setUserId( $userId )
	{
		$this->_userId = $userId;

		return $this;
	}
}